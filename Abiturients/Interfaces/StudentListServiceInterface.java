package Abiturients.Interfaces;

import Abiturients.Classes.Abiturient;
import Abiturients.Classes.StudentList;

public interface StudentListServiceInterface {
    Abiturient[] getNewStudents(Abiturient[] abiturients, int passScore, int countSVacantions);

    Abiturient[] sortAbiturients(Abiturient[] abiturients);

    int getAbiturientSumScore(Abiturient abiturient);
}
