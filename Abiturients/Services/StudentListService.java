package Abiturients.Services;

import Abiturients.Classes.Abiturient;
import Abiturients.Classes.StudentList;
import Abiturients.Classes.StudentMark;
import Abiturients.Interfaces.StudentListServiceInterface;

import java.util.Arrays;

public class StudentListService implements StudentListServiceInterface {

    @Override
    public Abiturient[] getNewStudents(Abiturient[] abiturients, int passScore, int countsVacantions) {
        Abiturient[] students = new Abiturient[countsVacantions];
        int j = 0;
        for (int i = abiturients.length - 1; i >= 0; i--) {
            if (abiturients[i].getSumScores() >= passScore && j <= countsVacantions) {
                students[j] = new Abiturient(abiturients[i].getName(), abiturients[i].getStudentMarks());
                j++;
            }
        }
        return students;
    }

    @Override
    public Abiturient[] sortAbiturients(Abiturient[] abiturients) {
        Abiturient[] sortedAbiturients = new Abiturient[abiturients.length];
        int[] arraySumPassScore = new int[abiturients.length];
        for (int i = 0; i < abiturients.length; i++) {
            int sum = 0;
            for (StudentMark studentMark : abiturients[i].getStudentMarks()) {
                sum += studentMark.getMark();
            }
            arraySumPassScore[i] = sum;
            abiturients[i].setSumScores(sum);
        }
        Arrays.sort(arraySumPassScore);
        for (int i = 0; i < arraySumPassScore.length; i++) {
            int j = 0;
            while (arraySumPassScore[i] != abiturients[j].getSumScores()) {
                j++;
            }
            sortedAbiturients[i] = new Abiturient(abiturients[j].getName(), abiturients[j].getStudentMarks());
            sortedAbiturients[i].setSumScores(arraySumPassScore[i]);
            abiturients[j].setSumScores(0);
        }
        return sortedAbiturients;
    }

    @Override
    public int getAbiturientSumScore(Abiturient abiturient) {
        int sum = 0;
        for (StudentMark studentMark : abiturient.getStudentMarks()) {
            sum += studentMark.getMark();
        }
        return sum;
    }
}
