package Abiturients.Services;

import Abiturients.Classes.Abiturient;
import Abiturients.Classes.StudentList;
import Abiturients.Classes.StudentMark;

public class DemoService {
    public static void startDemo() {
        Abiturient[] abiturients = new Abiturient[4];
        abiturients[0] = new Abiturient("Инокентий", new StudentMark[]{new StudentMark("Биология", 5), new StudentMark("Асторономия", 5), new StudentMark("Физкультура", 3)});
        abiturients[1] = new Abiturient("Еремей", new StudentMark[]{new StudentMark("Биология", 4), new StudentMark("Асторономия", 4), new StudentMark("Физкультура", 4)});
        abiturients[2] = new Abiturient("Святополк", new StudentMark[]{new StudentMark("Биология", 3), new StudentMark("Асторономия", 4), new StudentMark("Физкультура", 5)});
        abiturients[3] = new Abiturient("Вася", new StudentMark[]{new StudentMark("Биология", 3), new StudentMark("Асторономия", 3), new StudentMark("Физкультура", 5)});

        StudentList studentList = new StudentList(3, 12, abiturients);
        StudentListService studentListService = new StudentListService();
        Abiturient[] sortedAbiturient = new Abiturient[studentList.getAbiturients().length];
        Abiturient[] newStudents = new Abiturient[studentList.getCountNewStudents()];
        sortedAbiturient = studentListService.sortAbiturients(studentList.getAbiturients());
        newStudents = studentListService.getNewStudents(sortedAbiturient, studentList.getPass(), studentList.getCountNewStudents());

        for (int i = 0; i < newStudents.length; i++) {
            int sum = 0;
            for (StudentMark studentMark : newStudents[i].getStudentMarks()) {
                sum += studentMark.getMark();
            }
            newStudents[i].setSumScores(sum);
        }

        System.out.println("Поступили следующие абитуриенты:");
        for (Abiturient newStudent : newStudents) {
            System.out.println(newStudent.getName() + ":" + Integer.toString(newStudent.getSumScores()));
        }
    }
}
