package Abiturients;

import Abiturients.Services.DemoService;

/*
Имеется список абитуриентов (класс Abiturient) и список оценок, полученных ими на вступительных экзаменах.
Напечатать список поступивших, если число мест меньше числа абитуриентов.
Тестовые данные заполнить в коде, в виде массивов

 */

public class Main {
    public static void main(String[] args) {
        DemoService demoService = new DemoService();
        demoService.startDemo();
    }

}
