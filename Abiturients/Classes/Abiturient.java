package Abiturients.Classes;

public class Abiturient {
    private String name;
    private StudentMark[] studentMarks;
    private int sumScores;

    public Abiturient(String name, StudentMark[] studentMarks) {
        this.name = name;
        this.studentMarks = studentMarks;
    }

    public String getName() {
        return name;
    }

    public int getSumScores() {
        return this.sumScores;
    }

    public void setSumScores(int sumScores) {
        this.sumScores = sumScores;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StudentMark[] getStudentMarks() {
        return studentMarks;
    }

    public void setStudentMarks(StudentMark[] studentMarks) {
        this.studentMarks = studentMarks;
    }
}
