package Abiturients.Classes;

public class StudentList {
    private int countNewStudents;
    private int pass;
    private Abiturient[] abiturients;

    public StudentList(int countNewStudents, int pass, Abiturient[] abiturients) {
        this.countNewStudents = countNewStudents;
        this.pass = pass;
        this.abiturients = abiturients;
    }

    public int getCountNewStudents() {
        return countNewStudents;
    }

    public void setCountNewStudents(int countNewStudents) {
        this.countNewStudents = countNewStudents;
    }

    public int getPass() {
        return pass;
    }

    public void setPass(int pass) {
        this.pass = pass;
    }

    public Abiturient[] getAbiturients() {
        return abiturients;
    }

    public void setAbiturients(Abiturient[] abiturients) {
        this.abiturients = abiturients;
    }
}
