package Abiturients.Classes;

public class StudentMark {
    private String object;
    private int mark;

    public StudentMark(String object, int mark) {
        this.object = object;
        this.mark = mark;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
