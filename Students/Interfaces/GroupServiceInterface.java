package Students.Interfaces;

import Students.Classes.Group;

public interface GroupServiceInterface {
    float getAverageGroupMark(Group group);

    int getCountOfAStudents(Group group);

    int getCountFFStudents(Group group);
}
