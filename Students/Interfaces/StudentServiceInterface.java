package Students.Interfaces;

import Students.Classes.Student;

public interface StudentServiceInterface {
    float getAverageStudentMark(Student student);
}
