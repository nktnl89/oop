package Students.Classes;

public class Student {
    private StudentProgress[] studentProgresses;
    private String name;

    public Student(StudentProgress[] studentProgress, String name) {
        this.studentProgresses = studentProgress;
        this.name = name;
    }

    public StudentProgress[] getStudentProgresses() {
        return studentProgresses;
    }

    public void setStudentProgresses(StudentProgress[] studentProgresses) {
        this.studentProgresses = studentProgresses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
