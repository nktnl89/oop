package Students.Services;

import Students.Classes.Student;
import Students.Classes.StudentProgress;
import Students.Interfaces.StudentServiceInterface;

public class StudentService implements StudentServiceInterface {

    @Override
    public float getAverageStudentMark(Student student) {
        int sum = 0;
        StudentProgress[] studentProgresses = student.getStudentProgresses();
        for (int i = 0; i < studentProgresses.length; i++) {
            sum += studentProgresses[i].getMark();
        }
        return studentProgresses.length == 0 ? 0 : sum / studentProgresses.length;
    }
}
