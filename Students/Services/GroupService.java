package Students.Services;

import Students.Classes.Group;
import Students.Classes.Student;
import Students.Classes.StudentProgress;
import Students.Interfaces.GroupServiceInterface;

public class GroupService implements GroupServiceInterface {

    @Override
    public float getAverageGroupMark(Group group) {
        Student[] students = group.getStudents();
        int countMarks = 0;
        int groupSumMarks = 0;
        for (int i = 0; i < students.length; i++) {
            StudentProgress[] studentProgresses = students[i].getStudentProgresses();
            countMarks = studentProgresses.length;
            for (int j = 0; j < studentProgresses.length; j++) {
                groupSumMarks += studentProgresses[j].getMark();
            }
        }

        return students.length * countMarks != 0 ? groupSumMarks / (students.length * countMarks) : (float) 0;
    }

    @Override
    public int getCountOfAStudents(Group group) {
        Student[] students = group.getStudents();

        int countAStudents = 0;
        for (int i = 0; i < students.length; i++) {
            int sumStudentMarks = 0;
            StudentProgress[] studentProgresses = students[i].getStudentProgresses();
            for (int j = 0; j < studentProgresses.length; j++) {
                sumStudentMarks += studentProgresses[j].getMark();
            }
            if (sumStudentMarks == (5 * studentProgresses.length)) {
                countAStudents++;
            }
        }
        return countAStudents;
    }

    @Override
    public int getCountFFStudents(Group group) {
        Student[] students = group.getStudents();
        int countFStudents = 0;
        for (int i = 0; i < students.length; i++) {
            int sumStudentMarks = 0;
            StudentProgress[] studentProgresses = students[i].getStudentProgresses();
            for (int j = 0; j < studentProgresses.length; j++) {
                sumStudentMarks += studentProgresses[j].getMark();
            }
            if (sumStudentMarks <= (2 * studentProgresses.length)) {
                countFStudents++;
            }
        }
        return countFStudents;
    }


}
