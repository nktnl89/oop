package Students.Services;

import Students.Classes.Group;
import Students.Classes.Student;
import Students.Classes.StudentProgress;

public class DemoService {
    public static void StartDemo() {
        Student[] students = new Student[3];
        students[0] = new Student(new StudentProgress[]{new StudentProgress("Биология", 5), new StudentProgress("Астрономия", 5), new StudentProgress("Физкультура", 5)}, "Василий");
        students[1] = new Student(new StudentProgress[]{new StudentProgress("Биология", 3), new StudentProgress("Астрономия", 4), new StudentProgress("Физкультура", 4)}, "Еремей");
        students[2] = new Student(new StudentProgress[]{new StudentProgress("Биология", 1), new StudentProgress("Астрономия", 2), new StudentProgress("Физкультура", 2)}, "Святополк");

        Group group = new Group(students);

        System.out.println("Средний бал студентов:");
        StudentService studentService = new StudentService();

        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].getName() + " : " + Float.toString(studentService.getAverageStudentMark(students[i])));
        }

        GroupService groupService = new GroupService();
        System.out.println("Средняя оценка группы:");
        System.out.println(groupService.getAverageGroupMark(group));

        System.out.println("Количество отличников:");
        System.out.println(Integer.toString(groupService.getCountOfAStudents(group)));

        System.out.println("Количество двоечников:");
        System.out.println(Integer.toString(groupService.getCountFFStudents(group)));


    }
}
