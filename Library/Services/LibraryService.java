package Library.Services;

import Library.Classes.*;
import Library.Interfaces.LibraryServiceInterface;


public class LibraryService implements LibraryServiceInterface {
    @Override
    public void addBook(Library library, String bookName, String author) {
        Boolean vacantBook = false;
        Book[] libraryBooks = library.getLibraryBooks();
        for (int i = 0; i < libraryBooks.length; i++) {
            if (libraryBooks[i] == null) {
                libraryBooks[i] = new Book(bookName, author);
                vacantBook = true;
            }
        }
        if (vacantBook) {
            System.out.println("Книга " + bookName + " добавлена в библиотеку.");
        } else {
            System.out.println("Библиотека заполнена, удалите что-нибудь, прежде чем добавлять.");
        }

    }

    @Override
    public void deleteBook(Library library, String bookName) {
        Boolean bookFound = false;
        Book[] libraryBooks = library.getLibraryBooks();
        for (int i = 0; i < libraryBooks.length; i++) {
            if (libraryBooks[i].getName() == bookName) {
                libraryBooks[i] = null;
                bookFound = true;
            }
        }
        if (bookFound) {
            System.out.println("Книга " + bookName + " удалена.");
        } else {
            System.out.println("Книга с таким именем не найдена.");
        }
    }

    @Override
    public void searchBook(Library library, String bookName) {
        Boolean bookFound = false;
        Book[] libraryBooks = library.getLibraryBooks();
        for (int i = 0; i < libraryBooks.length; i++) {
            if (libraryBooks[i] == null) {
                continue;
            }
            if (libraryBooks[i].getName() == bookName) {
                System.out.println(libraryBooks[i].getName() + " найдена в библиотеке.");
                bookFound = true;
            }
        }
        if (!bookFound) {
            System.out.println("Книга с таким названием не найдена.");
        }
    }
}
