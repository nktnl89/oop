package Library.Services;

import Library.Classes.Book;
import Library.Classes.Library;
import Library.Interfaces.LibraryServiceInterface;

public class DemoService {

    public static void main(String args) {
        Library library = new Library();
        LibraryService libraryService = new LibraryService();
        Book[] booksArray = new Book[]{new Book("Война и мир", "Л.Н.Толстой"), new Book("Java для чайников", "Б.Берд")};
        library.setLibraryBooks(booksArray);

        libraryService.addBook(library, "Сказка о царе Салтане", "А.С.Пушкин");

        libraryService.deleteBook(library, "Война и мир");

        libraryService.searchBook(library, "Java для чайников");

        libraryService.addBook(library, "Сказка о царе Салтане", "А.С.Пушкин");
    }
}
