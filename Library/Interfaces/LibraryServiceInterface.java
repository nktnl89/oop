package Library.Interfaces;

import Library.Classes.Book;
import Library.Classes.Library;

public interface LibraryServiceInterface {
    void addBook(Library library, String bookName, String author);

    void deleteBook(Library library, String bookName);

    void searchBook(Library library, String bookName);
}
