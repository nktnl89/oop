package Library.Classes;

import Library.Interfaces.LibraryServiceInterface;
import Library.Classes.Book;

public class Library {

    private Book[] libraryBooks = new Book[3];

    public Library() {

    }

    public Book[] getLibraryBooks() {
        return libraryBooks;
    }

    public void setLibraryBooks(Book[] libraryBooks) {
        this.libraryBooks = libraryBooks;
    }
}
